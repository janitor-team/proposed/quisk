# These are the configuration parameters for Quisk using the
# SDR-IQ by RfSpace or the AMRAD Charleston Rx1 as the capture device.

# Please do not change this sample file.
# Instead copy it to your own .quisk_conf.py and make changes there.
# See quisk_conf_defaults.py for more information.

# Some decimation rates and resulting sampling frequencies:
#  Charleston decimation must be evenly divisible by four
#  Charleston 160 = 480k samples
#  Charleston 200 = 384k samples
#  Charleston 300 = 256k samples
#  Charleston 320 = 240k samples
#  Charleston 400 = 192k samples (DEFAULT)
#  Charleston 640 = 120k samples
#  Charleston 800 = 96k samples
#  Charleston 1600 = 48k samples
#  SDR-IQ decimation must be 360, 500, 600, or 1250
#  SDR-IQ 360 = 185k samples (185,185)
#  SDR-IQ 500 = 133k samples (133,333)
#  SDR-IQ 600 = 111k samples (111,111)
#  SDR-IQ 1250 = 53k samples (53,333)
 
Charleston = 1		# sets up the SDR-IQ or Charleston test

if Charleston == 1:
  #from charleston import quisk_widgets  # add Charleston widgets
  import charleston
  from charleston import quisk_hardware # Use different hardware file
  use_sdriq = 0							# do not use the SDR-IQ anymore
  sdriq_name = "Digilent"				# Digilent USB interface
  chas_rx1_clock = 76800000.0			# Charleston clock is 76.8MHz
  sdriq_decimation = 800
  sample_rate = int(float(chas_rx1_clock) / sdriq_decimation + 0.5)	# Don't change this
  display_fraction = 0.90				# the edges of the full bandwidth are not valid
  playback_rate = 48000			# Radio sound play rate
else:
  from sdriqpkg import quisk_hardware	# use SDR-IQ hardware file
  use_sdriq = 1							# Use the SDR-IQ for sound sampling setup
  sdriq_name = "/dev/ft2450"			# Name of the SDR-IQ device to open
  ##sdriq_name = "/dev/ttyUSB0"			# name of the SDR-IQ driver to open
  sdriq_clock = 66666667.0				# actual sample rate (66666667 nominal)
  sdriq_decimation = 500				# Must be 360, 500, 600, or 1250
  sample_rate = int(float(sdriq_clock) / sdriq_decimation + 0.5)	# Don't change this
  display_fraction = 0.85				# the edges of the full bandwidth are not valid

# The default hardware module is used for the SDR-IQ.

# In ALSA, soundcards have these names:
#name_of_sound_play = "hw:0"
#name_of_sound_play = "hw:1"
#name_of_sound_play = "plughw"
#name_of_sound_play = "plughw:1"
#name_of_sound_play = "default"

#use_sdriq = 1				# Use the SDR-IQ for sound sampling setup (also chas board)
#sample_rate = int(float(sdriq_clock) / sdriq_decimation + 0.5)	# Don't change this
name_of_sound_capt = ""			# We do not capture from the soundcard
name_of_sound_play = "pulse"		# Play back on this soundcard
# Note: For the SDR-IQ, playback is stereo at 48000 Hertz.
channel_i = 0				# Soundcard index of left channel
channel_q = 1				# Soundcard index of right channel

mouse_tune_method = 1			# tune freq unchanged
graph_refresh = 7
latency_millisecs = 150
filter_display = 1
#graph_y_scale = 80
#graph_y_zero  = 75

waterfall_y_scale = 90
waterfall_y_zero  = 70
waterfall_graph_y_scale = 100
waterfall_graph_y_zero = 70
